
import gspread
from google.oauth2.service_account import Credentials

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = Credentials.from_service_account_file('creds.json', scopes=scope)
gc = gspread.authorize(credentials)
ss = gc.open("NI Scrubs Orders")

worksheets = ['Belfast', 'Antrim & Newtownabbey', 'Armagh, Banbridge & Craigavon', 'Causeway Coast & Glens', 'Derry & Strabane', 'Fermanagh & Omagh', 'Lisburn & Castlereagh', 'Mid & East Antrim', 'Mid Ulster', 'Newry, Mourne & Down', 'North Down & Ards']
worksheets = dict(zip(range(len(worksheets)), worksheets))
import json

#@app.route("/submit/<sheet_id>", methods=['POST'])
def lambda_handler(event, context):
    print(event)
    if type(event) is dict:
        event = event['body']
    if type(event) is str:
        event = json.loads(event)
    sheet_id = [ x['value'] for x in event if x['name'] == 'sheet'][0]
    values = sorted([ x for x in event if x['name'] != 'sheet'], key=lambda y: y['name'])
    values = [x['value'] for x in values]
    sheet = [ s for s in ss.worksheets() if s.title == worksheets[int(sheet_id)] ][0]
    print(sheet)
    sheet.append_row(values)
    return '{}'

